import React, { useState } from 'react'
import styled from "styled-components"

// import UI material 
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';

import { selectCars } from '../features/car/carSlice';
import { useSelector } from 'react-redux';

const Header = () => {
    const [burgerStatus, setBurgerStatus] = useState(false)
    const cars = useSelector(selectCars);
    console.log(cars);

    return (
        <Container>
            <a>
                <img src="/images/logo.svg" alt="" />
            </a>

            <Menu>
                {cars && cars.map((car, index) => (
                    <a key={car || index || car.id} href="#">{car}</a>
                ))}
                {/* <a href="#">Model S</a>
                <a href="#">Model 3</a>
                <a href="#">Model X</a>
                <a href="#">Model Y</a> */}
            </Menu>

            <RihtMenu>
                <a href="#">Shop</a>
                <a href="#">Tesla Acount</a>
                <CustoMenu onClick={() => setBurgerStatus(true)} />
            </RihtMenu>
            <BurgerNav show={burgerStatus}>
                <CloseWrapper>
                    <CustomClose onClick={() => setBurgerStatus(false)} />
                </CloseWrapper>
                {cars && cars.map((car, index) => (
                    <li key={car || index || car.id}><a href="#">{car}</a></li>
                ))}
                <li><a href="#">Existing Inventory</a></li>
                <li><a href="#">Used Inventory</a></li>
                <li><a href="#">Trade-in</a></li>
                <li><a href="#">Cybertruck</a></li>
                <li><a href="#">Existing Inventory</a></li>
                <li><a href="#">Existing Inventory</a></li>
                <li><a href="#">Existing Inventory</a></li>
            </BurgerNav>

        </Container>

    )
}

export default Header

const Container = styled.div`
    min-height: 60px;
    position:fixed;
    display:flex;
    // background: red;
    width:100%;
    align-items:center;
    justify-content:space-between;
    padding: 0 20px;
    top:0;
    right:0;
    left:0;
    z-index:1
    `
const Menu = styled.div`
    display:flex;
    align-items:center;
    justify-content:center;
    flex:1;
    


    a{
        font-weight:600;
        text-transform: uppercase;
        padding: 0 10px;
        flex-wrap: nowrap;
        margin-left:4rem;
    }

    // responsive 
    @media(max-width:768px){
        display:none
    }

`

const RihtMenu = styled.div`
    display:flex;
    align-items: center;


a{
    font-weight:600;
    text-transform: uppercase;
    margin-right: 10px;
}
`

const CustoMenu = styled(MenuIcon)`
    cursor:pointer;
`

const BurgerNav = styled.div`
    position: fixed;
    top:0;
    right:0;
    Bottom:0;
    background: #F2E7D5;
    width:300px;
    z-index:16;
    list-style: none;
    padding: 20px;
    display:flex;
    flex-direction: column;
    text-align:start;
    transform: ${props => props.show ? 'traslateX(0)' : 'translateX(100%)'};
    transition: transform 0.2s ease-in;
    
    li{
        padding:15px 0 ;
        border-bottom: 1px solid rgba(0,0,0.2);

        a{

            font-weight: 600;
        }

    }
`


const CustomClose = styled(CloseIcon)`
    cursor:pointer;

`

// For reminder if you want to set an position in your elemenat even left or rigth you must wrap or inside the div container 
// to manipulate the posiiton  of it
const CloseWrapper = styled.div`
    dislpay:flex;
    text-align:end;

`


