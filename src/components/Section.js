import React from 'react'
import styled from 'styled-components'

// animation in rect reveal module 
import Fade from 'react-reveal/Fade';

const Section = ({ title, description, backgroundImg, leftBtnText, rightBtnText }) => {

    return (
        <Wrap bgImage={backgroundImg}>

            <Fade bottom>

                <ItemText>
                    <h1>{title}</h1>
                    <p>{description}</p>
                </ItemText>
                <Buttons>
                    <Fade bottom>
                        <ButtonGroup>
                            <LeftButton>{leftBtnText}</LeftButton>
                            {/* short ternary condtion on return true */}
                            {rightBtnText && <RightButton>{rightBtnText}</RightButton>}

                        </ButtonGroup>
                    </Fade>
                    < DownArwow src='/images/arrow.png' />

                </Buttons>
            </Fade>
        </Wrap>
    )
}

export default Section

const Wrap = styled.div`
    width:100vw;
    height:100vh;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    background-image: url('/images/model-s.jpg');
     display:flex;
     flex-direction: column;
    justify-content: space-between; // vertical
    align-items:center; //horizontal
    background-image:${props => ` url('/images/${props.bgImage}')`};
    
    `

const ItemText = styled.div`
    padding-top: 15vh;
    text-align: center;

`
const ButtonGroup = styled.div`
    display:flex;
    margin-bottom: 30px;
    @media (max-width: 768px){
            dislpay: flex;
            flex-direction: column; // responsive for buttom


    }

    <RightButton>{rightBtnText}</RightButton>


`

const LeftButton = styled.div`
        background-color: rgba(23, 26, 32, 0.8);
        height:40px;
        width: 256px;
        color:white;
        display: flex;
        justify-content: center;
        align-items:center;
        border-radius: 100px;
        opacity:0.85;
        text-transform:uppercase;
        font-size: 12px;
        cursor:pointer;
        margin:8px;
        
`


const RightButton = styled(LeftButton)`
background: white;
opacity: 0.65;
color: black;
`


const DownArwow = styled.img`
margin - top: 20px;
heightL 40px;
overflow - x: hidden;
animation: animateDown infinite 1.5s;
font-size: 10px;
background-color: white;
opacity: 0.65;
border-radius: 100px;

`

const Buttons = styled.div``
